#include <vector>

std::vector<int> calc_primes(const int max) {
    std::vector<int> sieve;
    std::vector<int> primes;

    for (int i = 1; i < max + 1; ++i)
        sieve.push_back(i);   
    sieve[0]=0;
    
    for (int i = 2; i < max + 1; ++i) {   
        if (sieve[i-1] != 0) {
            primes.push_back(sieve[i-1]);
        for (int j = 2 * sieve[i-1]; j < max + 1; j += sieve[i-1])
            sieve[j-1] = 0;
        }
    }
    return primes;
}
