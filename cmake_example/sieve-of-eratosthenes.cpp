#include <iostream>
#include <vector>
#include <chrono>

#include "calc_primes_sieve.h"
#include "log.h"

typedef std::chrono::high_resolution_clock Clock;

//std::vector<int> calc_primes(const int max);

int main(int argc, char *argv[]) {
    
    // Program Execution time
    auto t1 = Clock::now();

    const int max = 1000001;
    int count = 0;
    int biggest = 0;

    std::vector<int> primes = calc_primes(max);

    for(int i = 0; i < primes.size(); i++)
    {
        if(primes[i] != 0) {
            count += 1;
            biggest = primes[i];
            //std::cout << primes[i] << std::endl;
        }
    }
    
    FILELog::ReportingLevel() = logDEBUG3;
    FILE* log_fd = fopen( "mylogfile.txt", "w" );
    Output2FILE::Stream() = log_fd;

    double expectedX = 0.0;
    double realX = 1.0;

    FILE_LOG(logWARNING) << "Ops, variable x should be " << expectedX << "; is " << realX;

    //Log::ReportingLevel() = logDEBUG2;
    //const int logcount = 3;
    //Log().Get(logDEBUG) << "A loop with "    << logcount << " iterations";
    //for (int i = 0; i != logcount; ++i)
    //{
    //    Log().Get(logDEBUG1)        << "the counter i = " << i;
    //}
    
    std::cout << "There are " << count << " primes less than " << max << " and the biggest of them is " << biggest << std::endl;

    // Program Execution times
    auto t2 = Clock::now();
    std::cout << "Program execution time was = "
              << std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count()
              << " microseconds" << std::endl;
    return 0;
}

