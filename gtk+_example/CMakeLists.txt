cmake_minimum_required (VERSION 3.6)
project (Example_GTK)
# The version number
set (Example_GTK_VERSION_MAJOR 0)
set (Example_GTK_VERSION_MINOR 1)

#use pkg config to get GTK+ headers and library files
find_package(PkgConfig REQUIRED)
pkg_check_modules(GTK REQUIRED gtk+-3.0)

include_directories(${GTK_INCLUDE_DIRS})
link_directories(${GTK_LIBRARY_DIRS})
add_definitions(${GTK_CFLAGS_OTHER})
# Set the output folder where your program will be created
set(CMAKE_BINARY_DIR ${CMAKE_SOURCE_DIR}/bin)
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR})
set(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR})

# show all the compiler errors and warnings
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1y -Wall")

# add the executable
add_executable(	example0.bin 
		example-0.cpp)

target_link_libraries(example0.bin ${GTK_LIBRARIES})            
