#include <iostream>
#include "log.h"

int main(int argc, char* argv[])
{
	FILELog::ReportingLevel() = logDEBUG;
    FILE* log_fd = fopen( "mylogfile.txt", "w" );
    Output2FILE::Stream() = log_fd;

    FILE_LOG(logERROR) << "This is a log ERROR";
	FILE_LOG(logWARNING) << "This is a log WARNING";
	FILE_LOG(logINFO) << "This is a log INFO";
	FILE_LOG(logDEBUG) << "This is a log DEBUG";
	FILE_LOG(logDEBUG1) << "This is a log DEBUG1";
	FILE_LOG(logDEBUG2) << "This is a log DEBUG2";
	FILE_LOG(logDEBUG3) << "This is a log DEBUG3";
	FILE_LOG(logDEBUG4) << "This is a log DEBUG4";
	FILE_LOG(logERROR) << "This is another log ERROR";
	FILE_LOG(logWARNING) << "This is another log WARNING";
	FILE_LOG(logINFO) << "This is another log INFO";
	FILE_LOG(logDEBUG) << "This is another log DEBUG";
	FILE_LOG(logDEBUG1) << "This is another log DEBUG1";
	FILE_LOG(logDEBUG2) << "This is another log DEBUG2";
	FILE_LOG(logDEBUG3) << "This is another log DEBUG3";
	FILE_LOG(logDEBUG4) << "This is another log DEBUG4";
	return 0;
	
}
